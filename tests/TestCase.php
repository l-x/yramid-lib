<?php

declare(strict_types=1);

namespace Yramid\Test;

use Closure;
use PHPUnit\Framework\TestCase as PhpUnitTestCase;
use Yramid\Config;
use Yramid\ConfigAccessor;
use Yramid\Migration\MigrationData;
use Yramid\Migration\MigrationStatus;
use Yramid\Yramid;

abstract class TestCase extends PhpUnitTestCase
{
    protected Yramid $subject;

    protected Config $config;

    abstract protected function getConfig(): Config;

    protected function setUp(): void
    {

        parent::setUp();

        $this->subject = new Yramid();
        $this->config = $this->getConfig();
    }

    protected function assertStatus(array $expected): void
    {
        $this->assertSame(
            $expected,
            array_column(
                $this->subject->getMigrationStatus($this->config),
                'status',
                'serial',
            ),
        );
    }

    protected function assertTestLog(array $expected): void
    {
        $this->assertSame(
            $expected,
            ConfigAccessor::getPdo($this->config)
                ->query('SELECT * FROM test_log')
                ->fetchAll(\PDO::FETCH_COLUMN),
        );
    }

    protected function postMigrateCallback(?array &$log = null): Closure
    {
        $log ??= [];

        return static function (
            MigrationData $migrationData,
            MigrationStatus $oldStatus,
            MigrationStatus $newStatus,
            float $runTime,
        ) use (&$log): void {
            $log[$migrationData->serial] = [
                $oldStatus,
                $newStatus
            ];
        };
    }
}
