<?php

declare(strict_types=1);

namespace Yramid\Test;

use PDO;
use Yramid\Config;

class TestConfig implements Config
{
    private ?PDO $pdo;

    public function getPdo(): PDO
    {
        return $this->pdo ??= new PDO('sqlite::memory:');
    }

    public function getMigrationPath(): string
    {
        return "vfs://root/migrations";
    }

    public function getMigrationNamespace(): string
    {
        return "Yramid\Test\Migration";
    }

    public function getMigrationTemplate(): ?string
    {
        return null;
    }

    public function getSeedPath(): string
    {
        return "vfs://root/seeds";
    }

    public function getSeedNamespace(): string
    {
        return "Yramid\Test\Seeds";
    }

    public function getSeedTemplate(): ?string
    {
        return null;
    }
}
