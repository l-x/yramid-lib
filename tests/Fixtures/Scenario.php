<?php

declare(strict_types=1);

namespace Yramid\Test\Fixtures;

use org\bovigo\vfs\vfsStream;
use Yramid\Config;
use Yramid\ConfigAccessor;
use Yramid\Test\TestConfig;
use Yramid\Yramid;

class Scenario
{
    public static function create(
        array $logEntries,
        array $seedPathContents,
        array $migrationPathContents,
    ): Config {
        $fsRoot = vfsStream::setup('root');

        $config = new TestConfig();
        (new Yramid())->setUp($config);

        ConfigAccessor::getPdo($config)->query('CREATE TABLE IF NOT EXISTS test_log (method TEXT)');


        $insert = ConfigAccessor::getPdo($config)->prepare(
            'INSERT INTO yramid_log SELECT ?, ?, ?, ?',
        );
        foreach ($logEntries as $logEntry) {
            $insert->execute($logEntry);
        }

        vfsStream::create(
            [
                'seeds' => $seedPathContents,
                'migrations' => $migrationPathContents,
            ],
            $fsRoot,
        );

        return $config;
    }

    public static function migrationClass(string $name, string $body = ''): string
    {
        return self::mockClass(
            ConfigAccessor::getMigrationNamespace(new TestConfig()),
            $name,
            $body,
        );
    }

    public static function seedClass(string $name, string $body = ''): string
    {
        return self::mockClass(
            ConfigAccessor::getSeedNamespace(new TestConfig()),
            $name,
            $body,
        );
    }

    private static function mockClass(
        string $namespace,
        string $name,
        string $body
    ): string {
        return <<<PHP
        <?php

        namespace $namespace;

        class $name implements \Yramid\Seed, \Yramid\Migration
        {
            use \Yramid\Test\Fixtures\LogTrait;

            $body
        }
        PHP;
    }
}
