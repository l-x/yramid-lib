<?php

declare(strict_types=1);

namespace Yramid\Test\Fixtures;

use PDO;

trait LogTrait
{
    private static function log(PDO $pdo, string $method): void
    {
        $shortCaller = (new \ReflectionClass(static::class))->getShortName();
        $stmt = $pdo->prepare('INSERT INTO test_log (method) VALUES (?)');
        $stmt->execute([$shortCaller . '::' . $method]);
    }

    public static function run(PDO $pdo): void
    {
        self::log($pdo, __FUNCTION__);
    }

    public static function up(PDO $pdo): void
    {
        self::log($pdo, __FUNCTION__);
    }

    public static function down(PDO $pdo): void
    {
        self::log($pdo, __FUNCTION__);
    }
}
