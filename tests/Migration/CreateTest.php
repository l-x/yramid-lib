<?php

declare(strict_types=1);

namespace Yramid\Test\Migration;

use Yramid\Exception\InvalidName;
use Yramid\Exception\LogicException;
use Yramid\Config;
use Yramid\Exception\RuntimeException;
use Yramid\Migration;
use Yramid\Test\Fixtures\Scenario;
use Yramid\Test\TestCase;

/**
 * @testdox Yramid
 */
class CreateTest extends TestCase
{
    protected function getConfig(): Config
    {
        return Scenario::create(
            [],
            [],
            [
                '100_Migration100.php' => Scenario::migrationClass('Migration100'),
            ],
        );
    }

    /**
     * @test
     * @testdox createMigration() creates a valid migration stub class
     */
    public function createMigration(): void
    {
        $serial = random_int(10_000, 20_000);

        $migrationData = $this->subject->createMigration(
            $this->config,
            $serial,
            "Migration$serial",
        );

        $this->assertFileExists($migrationData->fileName);
        require $migrationData->fileName;

        $this->assertTrue(class_exists($migrationData->className));
        $this->assertTrue(
            is_a(
                $migrationData->className,
                Migration::class,
                true,
            ),
        );
    }

    /**
     * @test
     * @testdox createMigration() fails on colliding serial
     */
    public function createMigrationCollision(): void
    {
        $serial = random_int(10_000, 20_000);

        $migrationData = $this->subject->createMigration(
            $this->config,
            $serial,
            "MigrationOne$serial",
        );

        $this->expectExceptionObject(
            new LogicException('There is already a migration with serial ' . $serial),
        );

        $migrationData = $this->subject->createMigration(
            $this->config,
            $serial,
            "MigrationTwo$serial",
        );

        $this->assertFileExists($migrationData->fileName);
        require $migrationData->fileName;

        $this->assertTrue(class_exists($migrationData->className));
        $this->assertTrue(
            is_a(
                $migrationData->className,
                Migration::class,
                true,
            ),
        );
    }

    /**
     * @test
     * @testdox createMigration() fails on invalid name
     */
    public function createSeedInvalidName(): void
    {
        $this->expectExceptionObject(new InvalidName('invalid_migration'));
        $this->subject->createMigration(
            $this->config,
            1224,
            'invalid_migration',
        );
    }
}
