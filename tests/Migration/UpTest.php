<?php

declare(strict_types=1);

namespace Yramid\Test\Migration;

use Yramid\ConfigAccessor;
use Yramid\Exception\LogicException;
use Yramid\Config;
use Yramid\Exception\UnknownSerial;
use Yramid\Migration\MigrationStatus;
use Yramid\Test\Fixtures\Scenario;
use Yramid\Test\TestCase;

/**
 * @testdox Yramid
 */
class UpTest extends TestCase
{
    protected function getConfig(): Config
    {
        return Scenario::create(
            [
                [100, 'Migration100', date('c'), null],
                [300, 'Migration300', date('c'), null],
                [500, 'Migration500', date('c'), null],
                [600, 'Migration600', date('c'), null], // <- missing
                [700, 'Migration700', null, 'TestSavePoint'],
                // [900, 'Migration900', null, null], // <- new
            ],
            [],
            [
                '123_InvalidFile.sql' => '',
                '100_Migration100.php' => Scenario::migrationClass('Migration100'),
                '300_Migration300.php' => Scenario::migrationClass('Migration300'),
                '500_Migration500.php' => Scenario::migrationClass('Migration500'),
                'subidr' => ['700_Migration700.php' => Scenario::migrationClass('Migration700')],
                '900_Migration900.php' => Scenario::migrationClass('Migration900'),
            ],
        );
    }

    /**
     * @test
     * @testdox migrateUp() fails on invalid serial
     */
    public function migrateUpInvalidSerial(): void
    {
        $this->expectExceptionObject(new UnknownSerial(999));
        $this->subject->migrateUp(
            $this->config,
            999,
            false,
        );
    }

    /**
     * @test
     * @testdox migrateUp() fails on invalid serial
     */
    public function migrateFailsOnInvalidFile(): void
    {
        file_put_contents(
            ConfigAccessor::getMigrationPath($this->config) . '/801_Fail801.php',
            '<?php namespace ' . ConfigAccessor::getMigrationNamespace($this->config) . '; class Fail801 {}',
        );

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Class \'Yramid\Test\Migration\Fail801\' is invalid or does not exist');
        $this->subject->migrateUp(
            $this->config,
            801,
            false,
        );
    }

    /**
     * @test
     * @testdox migrateUp() works as expected
     */
    public function migrateUp(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->subject->migrateUp(
            $this->config,
            900,
            false,
            $this->postMigrateCallback($callbackLog),
        );

        $this->assertSame(
            [
                100 => [MigrationStatus::UP, MigrationStatus::UP],
                300 => [MigrationStatus::UP, MigrationStatus::UP],
                500 => [MigrationStatus::UP, MigrationStatus::UP],
                600 => [MigrationStatus::MISSING, MigrationStatus::MISSING],
                700 => [MigrationStatus::DOWN, MigrationStatus::UP],
                900 => [MigrationStatus::NEW, MigrationStatus::UP],
            ],
            $callbackLog,
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::UP,
            900 => MigrationStatus::UP,
        ]);

        $this->assertTestLog([
            'Migration700::up',
            'Migration900::up',
        ]);
    }

    /**
     * @test
     * @testdox migrateUp() works as expected - dry-run
     */
    public function migrateUpDryRun(): void
    {
        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->subject->migrateUp(
            $this->config,
            900,
            true,
            $this->postMigrateCallback($callbackLog),
        );

        $this->assertSame(
            [
                100 => [MigrationStatus::UP, MigrationStatus::UP],
                300 => [MigrationStatus::UP, MigrationStatus::UP],
                500 => [MigrationStatus::UP, MigrationStatus::UP],
                600 => [MigrationStatus::MISSING, MigrationStatus::MISSING],
                700 => [MigrationStatus::DOWN, MigrationStatus::UP],
                900 => [MigrationStatus::NEW, MigrationStatus::UP],
            ],
            $callbackLog,
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            900 => MigrationStatus::NEW,
        ]);

        $this->assertTestLog([]);
    }

    /**
     * @test
     * @testdox migrateUp() rolls back everything on exception
     */
    public function migrateUpException(): void
    {
        file_put_contents(
            ConfigAccessor::getMigrationPath($this->config) . '/800_Fail800.php',
            Scenario::migrationClass(
                'Fail800',
                'public static function up(\PDO $pdo): void { throw new \RuntimeException(__METHOD__); }',
            ),
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            800 => MigrationStatus::NEW,
            900 => MigrationStatus::NEW,
        ]);

        $callbackLog = [];

        try {
            $this->subject->migrateUp(
                $this->config,
                900,
                false,
                $this->postMigrateCallback($callbackLog),
            );
            $this->fail('No exception thrown');
        } catch (\RuntimeException) {
            // everything ok
        }

        $this->assertSame(
            [
                100 => [MigrationStatus::UP, MigrationStatus::UP],
                300 => [MigrationStatus::UP, MigrationStatus::UP],
                500 => [MigrationStatus::UP, MigrationStatus::UP],
                600 => [MigrationStatus::MISSING, MigrationStatus::MISSING],
                700 => [MigrationStatus::DOWN, MigrationStatus::UP],
            ],
            $callbackLog,
        );

        $this->assertStatus([
            100 => MigrationStatus::UP,
            300 => MigrationStatus::UP,
            500 => MigrationStatus::UP,
            600 => MigrationStatus::MISSING,
            700 => MigrationStatus::DOWN,
            800 => MigrationStatus::NEW,
            900 => MigrationStatus::NEW,
        ]);

        $this->assertTestLog([]);
    }
}
