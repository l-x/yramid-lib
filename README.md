[![Codeberg](https://img.shields.io/badge/Codeberg-yramid-green?logo=codeberg&style=for-the-badge)](https://codeberg.org/yramid/lib)
[![Packagist License](https://img.shields.io/packagist/l/yramid/lib?style=for-the-badge)](https://packagist.org/packages/yramid/lib)
[![Packagist PHP Version Support](https://img.shields.io/packagist/php-v/yramid/lib?style=for-the-badge)](https://packagist.org/packages/yramid/lib)
[![Packagist Version](https://img.shields.io/packagist/v/yramid/lib?style=for-the-badge)](https://packagist.org/packages/yramid/lib)

# `yramid/lib`

`yramid/lib` is a zero-dependency and easy to use database migration library for php 8.1 and above.

This library encapsulates the business logic for [`yramid/symfony-commands`](https://codeberg.org/yramid/symfony-commands) and [`yramid/cli`](https://codeberg.org/yramid/cli).

In most of the cases `yramid/lib` will not have any practical use for you - except you want to write your own database migration tool.
