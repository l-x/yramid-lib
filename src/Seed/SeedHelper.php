<?php

declare(strict_types=1);

namespace Yramid\Seed;

use Yramid\ConfigAccessor;
use Yramid\Exception\RuntimeException;
use Throwable;
use Yramid\Config;

/**
 * @internal
 */
final class SeedHelper
{
    public static function run(Config $config, string $name, bool $dryRun): void
    {
        $seeds = SeedStorage::list($config);
        $seed = $seeds[$name] ?? null;

        if (!$seed) {
            throw new RuntimeException('Unknown Seed ' . $name);
        }

        /** @psalm-suppress UnresolvableInclude */
        require_once $seed->fileName;

        $pdo = ConfigAccessor::getPdo($config);
        $pdo->beginTransaction();

        try {
            ($seed->className)::run($pdo);
        } catch (Throwable $throwable) {
            $pdo->rollBack();
            throw $throwable;
        }

        if ($dryRun) {
            $pdo->rollBack();
        } else {
            $pdo->commit();
        }
    }
}
