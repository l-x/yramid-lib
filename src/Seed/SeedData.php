<?php

declare(strict_types=1);

namespace Yramid\Seed;

use Yramid\Seed;

/**
 * @api
 */
final class SeedData
{
    /**
     * @param non-empty-string $name
     * @param non-empty-string $fileName
     * @param class-string<Seed> $className
     */
    public function __construct(
        public readonly string $name,
        public readonly string $fileName,
        public readonly string $className,
    ) {
    }
}
