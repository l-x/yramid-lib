<?php

declare(strict_types=1);

namespace #NAMESPACE#;

use PDO;
use Yramid\Seed;

class #NAME# implements Seed
{
    public static function run(PDO $pdo): void
    {
        // @todo Implement #NAME#::run()
    }
}
