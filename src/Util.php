<?php

declare(strict_types=1);

namespace Yramid;

use Yramid\Exception\InvalidName;

/**
 * @internal
 */
final class Util
{
    private const CAMEL_CASE = '{^[A-Z][A-Za-z0-9]{2,}$}';

    /**
     * @param string $name
     *
     * @return non-empty-string
     * @throws InvalidName
     */
    public static function filterName(string $name): string
    {
        if (!preg_match(self::CAMEL_CASE, $name)) {
            InvalidName::raise($name);
        }

        /** @var non-empty-string $name */
        return $name;
    }

    /**
     * @param string $name
     * @param string $namespace
     * @param string $templateFile
     * @param array $replacements
     *
     * @return array{class-string, non-empty-string}
     */
    public static function createClass(
        string $name,
        string $namespace,
        string $templateFile,
        array $replacements = [],
    ): array {
        $name = self::filterName($name);

        $replacements += [
            '#NAME#' => $name,
            '#NAMESPACE#' => $namespace,
            '#TIMESTAMP#' => date('c'),
        ];

        /** @var non-empty-string $body */
        $body = strtr(
            file_get_contents($templateFile),
            $replacements,
        );

        //echo "$body\n\n\n";

        /** @var class-string $fqcn */
        $fqcn = "$namespace\\$name";

        return [$fqcn, $body];
    }
}
