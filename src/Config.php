<?php

declare(strict_types=1);

namespace Yramid;

use PDO;

/**
 * @api
 */
interface Config
{
    /**
     * Return the \PDO instance to be used by the library
     *
     * This method is called multiple times within a common migration workflow.
     * Make sure this method **ALWAYS** returns the **SAME INSTANCE** (aka same database connection).
     * Do not simply do things like `return new \PDO($dsn);`, otherwise your migrations and the logging
     * are running in a separate transaction each which may put your database in an unpredictable state.
     *
     * @return PDO
     */
    public function getPdo(): PDO;

    /**
     * Return the filesystem path for migration files
     *
     * Migration files can be organised in subdirectories of this path.
     *
     * @return string
     */
    public function getMigrationPath(): string;

    /**
     * Return the namespace to be used for migration classes
     *
     * Must be a valid namespace identifier without leading and trailing backslash
     *
     * @return string
     */
    public function getMigrationNamespace(): string;

    /**
     * Return the template file for migration files/classes
     *
     * @return string|null
     */
    public function getMigrationTemplate(): ?string;


    /**
     * Return the filesystem path for seed files
     *
     * Seed files can be organised in subdirectories of this path.
     *
     * @return string
     */
    public function getSeedPath(): string;

    /**
     * Return the namespace to be used for seed classes
     *
     * Must be a valid namespace identifier without leading and trailing backslash
     *
     * @return string
     */
    public function getSeedNamespace(): string;

    /**
     * Return the template file for seed files/classes
     *
     * @return string|null
     */
    public function getSeedTemplate(): ?string;
}
