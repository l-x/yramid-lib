<?php

declare(strict_types=1);

namespace Yramid;

use Closure;
use Yramid\Migration\MigrationData;
use Yramid\Migration\MigrationHelper;
use Yramid\Migration\MigrationLogs;
use Yramid\Migration\MigrationStorage;
use Yramid\Seed\SeedData;
use Yramid\Seed\SeedHelper;
use Yramid\Seed\SeedStorage;

/**
 * @api
 */
final class Yramid implements YramidInterface
{
    public function setUp(Config $config): void
    {
        SeedStorage::setUp($config);
        MigrationLogs::setUp($config);
        MigrationStorage::setUp($config);
    }

    /**
     * @inheritDoc
     */
    public function createSeed(Config $config, string $name): SeedData
    {
        return SeedStorage::create(
            $config,
            $name,
        );
    }

    /**
     * @inheritDoc
     */
    public function runSeed(Config $config, string $name, bool $dryRun): void
    {
        SeedHelper::run(
            $config,
            $name,
            $dryRun,
        );
    }

    /**
     * @inheritDoc
     */
    public function createMigration(Config $config, int $serial, string $name): MigrationData
    {
        return MigrationStorage::create(
            $config,
            $serial,
            $name,
        );
    }

    /**
     * @inheritDoc
     */
    public function migrateUp(
        Config $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void {
        MigrationHelper::up(
            $config,
            $target,
            $dryRun,
            $postMigrationCallback,
        );
    }

    /**
     * @inheritDoc
     */
    public function migrateDown(
        Config $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void {
        MigrationHelper::down(
            $config,
            $target,
            $dryRun,
            $postMigrationCallback,
        );
    }

    /**
     * @inheritDoc
     */
    public function getMigrationStatus(Config $config): array
    {
        return MigrationHelper::getStatus(
            $config,
        );
    }

    /**
     * @inheritDoc
     */
    public function setMigrationSavepoint(
        Config $config,
        int|string $target,
        ?string $savepoint,
    ): void {
        MigrationLogs::setSavepoint(
            $config,
            $target,
            $savepoint,
        );
    }
}
