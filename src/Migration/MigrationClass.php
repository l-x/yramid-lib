<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Yramid\Migration;

/**
 * @internal
 */
final class MigrationClass
{
    /**
     * @param positive-int $serial
     * @param non-empty-string $name
     * @param non-empty-string $fileName
     * @param class-string<Migration> $className
     */
    public function __construct(
        public readonly int $serial,
        public readonly string $name,
        public readonly string $fileName,
        public readonly string $className,
    ) {
    }
}
