<?php

declare(strict_types=1);

namespace Yramid\Migration;

/**
 * @internal
 */
final class MigrationLog
{
    /**
     * @param positive-int $serial
     * @param non-empty-string $name
     * @param non-empty-string|null $timestamp
     * @param non-empty-string|null $savepoint
     */
    public function __construct(
        public readonly int $serial,
        public readonly string $name,
        public readonly ?string $timestamp,
        public readonly ?string $savepoint,
    ) {
    }
}
