<?php

declare(strict_types=1);

namespace Yramid\Migration;

use FilesystemIterator;
use Yramid\ConfigAccessor;
use Yramid\Exception\LogicException;
use SplFileInfo;
use Yramid\Config;
use Yramid\Migration;
use Yramid\Util;

/**
 * @internal
 */
final class MigrationStorage
{
    public static function setUp(Config $config): void
    {
        $migrationPath = ConfigAccessor::getMigrationPath($config);

        if (is_dir($migrationPath)) {
            return;
        }

        mkdir(
            $migrationPath,
            recursive: true,
        );
    }

    /**
     * @param string $path
     * @param string $namespace
     * @param array<positive-int, MigrationClass> $agg
     *
     * @return array<positive-int, MigrationClass>
     */
    private static function doList(string $path, string $namespace, array $agg): array
    {
        $fileIterator = new FilesystemIterator(
            $path,
            FilesystemIterator::SKIP_DOTS,
        );

        /** @var SplFileInfo $fileInfo */
        foreach ($fileIterator as $fileInfo) {
            /** @var non-empty-string $pathName */
            $pathName = $fileInfo->getPathname();

            if ($fileInfo->isFile()) {
                $isFileNameValid = preg_match(
                    '/^(?<serial>[1-9]\d*)_(?<name>[a-zA-Z0-9]+)\.php$/i',
                    basename($pathName),
                    $matches,
                );

                if (!$isFileNameValid) {
                    continue;
                }

                /** @var positive-int $serial */
                $serial = (int) $matches['serial'];

                /** @var non-empty-string $name */
                $name = $matches['name'];

                /** @var class-string<Migration> $className */
                $className = "$namespace\\$name";

                $agg[$serial] = new MigrationClass(
                    $serial,
                    $name,
                    $pathName,
                    $className,
                );
            } elseif ($fileInfo->isDir()) {
                $agg = self::doList($pathName, $namespace, $agg);
            }
        }

        return $agg;
    }

    /**
     * @param Config $config
     *
     * @return array<positive-int, MigrationClass>
     */
    public static function list(Config $config): array
    {
        $list = self::doList(
            ConfigAccessor::getMigrationPath($config),
            ConfigAccessor::getMigrationNamespace($config),
            [],
        );

        ksort($list);

        return $list;
    }

    /**
     * @param Config $config
     * @param positive-int $serial
     * @param non-empty-string $name
     *
     * @return MigrationData
     */
    public static function create(Config $config, int $serial, string $name): MigrationData
    {
        $existingMigrations = self::list($config);

        if (isset($existingMigrations[$serial])) {
            throw new LogicException('There is already a migration with serial ' . $serial);
        }

        $migrationNamespace = ConfigAccessor::getMigrationNamespace($config);
        $name = Util::filterName($name);

        $migrationPath = ConfigAccessor::getMigrationPath($config);
        $fileName = "{$migrationPath}/{$serial}_{$name}.php";

        /**
         * @psalm-suppress UnnecessaryVarAnnotation
         * @var class-string<Migration> $className
         */
        [$className, $body] = Util::createClass(
            $name,
            $migrationNamespace,
            ConfigAccessor::getMigrationTemplate($config),
            ['#SERIAL#' => $serial],
        );

        file_put_contents(
            $fileName,
            $body,
        );

        return new MigrationData(
            $serial,
            $name,
            MigrationStatus::NEW,
            null,
            null,
            $fileName,
            $className,
        );
    }
}
