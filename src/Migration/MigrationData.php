<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Yramid\Migration;

/**
 * @api
 */
class MigrationData
{
    /**
     * @param positive-int $serial
     * @param non-empty-string|string $name
     * @param MigrationStatus $status
     * @param non-empty-string|null $timestamp
     * @param non-empty-string|null $savepoint
     * @param non-empty-string|null $fileName
     * @param class-string<Migration>|null $className
     */
    public function __construct(
        public readonly int $serial,
        public readonly string $name,
        public readonly MigrationStatus $status,
        public readonly ?string $timestamp,
        public readonly ?string $savepoint,
        public readonly ?string $fileName,
        public readonly ?string $className,
    ) {
    }
}
