<?php

declare(strict_types=1);

namespace Yramid\Migration;

// @codingStandardsIgnoreStart

/**
 * @api
 */
enum MigrationStatus
{
    case NEW;
    case UP;
    case DOWN;
    case MISSING;
}
