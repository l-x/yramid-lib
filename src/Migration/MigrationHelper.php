<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Closure;
use Throwable;
use Yramid\Config;
use Yramid\ConfigAccessor;
use Yramid\Exception\LogicException;
use Yramid\Exception\UnknownSerial;
use Yramid\Migration;

/**
 * @internal
 */
final class MigrationHelper
{
    /**
     * @param Config $config
     * @param positive-int|non-empty-string $target
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     *
     * @throws Throwable
     */
    public static function up(
        Config $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void {
        $targetSerial = is_string($target)
            ? MigrationLogs::getSerialFor($config, $target)
            : $target;

        self::migrateUp(
            $config,
            $targetSerial,
            $dryRun,
            $postMigrationCallback,
        );
    }

    /**
     * @param Config $config
     * @param positive-int $targetSerial
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     *
     * @throws Throwable
     */
    private static function migrateUp(
        Config $config,
        int $targetSerial,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void {
        $migrations = self::getStatus($config);
        $targetMigration = $migrations[$targetSerial] ?? null;

        if (!$targetMigration) {
            UnknownSerial::raise($targetSerial);
        }

        $pdo = ConfigAccessor::getPdo($config);
        $pdo->beginTransaction();

        try {
            while ($migration = array_shift($migrations)) {
                $migrationStatus = $migration->status;

                if ($migrationStatus === MigrationStatus::DOWN || $migrationStatus == MigrationStatus::NEW) {
                    /** @psalm-suppress  UnresolvableInclude */
                    require_once $migration->fileName;

                    if (!is_a($migration->className, Migration::class, true)) {
                        throw new LogicException("Class '$migration->className' is invalid or does not exist");
                    }

                    ($migration->className)::up($pdo);

                    $migrationStatusNew = MigrationStatus::UP;

                    $timeStart = microtime(true);
                    MigrationLogs::up(
                        $config,
                        $migration,
                    );
                    $runTime = microtime(true) - $timeStart;
                } else {
                    $migrationStatusNew = $migrationStatus;
                    $runTime = .0;
                }

                $postMigrationCallback && $postMigrationCallback(
                    $migration,
                    $migrationStatus,
                    $migrationStatusNew,
                    $runTime,
                );

                if ($migration->serial === $targetSerial) {
                    break;
                }
            }
        } catch (Throwable $throwable) {
            $pdo->rollBack();
            throw $throwable;
        }

        if ($dryRun) {
            $pdo->rollBack();
        } else {
            $pdo->commit();
        }
    }

    /**
     * @param Config $config
     * @param positive-int|non-empty-string $target
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     * @throws Throwable
     */
    public static function down(
        Config $config,
        int|string $target,
        bool $dryRun,
        ?closure $postMigrationCallback = null,
    ): void {
        $targetSerial = is_string($target)
            ? MigrationLogs::getNextSerialFor($config, $target)
            : $target;

        if (!$targetSerial) {
            return;
        }

        self::migrateDown(
            $config,
            $targetSerial,
            $dryRun,
            $postMigrationCallback,
        );
    }

    /**
     * @param Config $config
     * @param positive-int $targetSerial
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     * @throws Throwable
     */
    private static function migrateDown(
        Config $config,
        int $targetSerial,
        bool $dryRun,
        ?closure $postMigrationCallback = null,
    ): void {
        $migrations = self::getStatus($config);
        $targetMigration = $migrations[$targetSerial] ?? null;

        if (!$targetMigration) {
            UnknownSerial::raise($targetSerial);
        }

        $pdo = ConfigAccessor::getPdo($config);
        $pdo->beginTransaction();

        try {
            while ($migration = array_pop($migrations)) {
                if ($migration->status === MigrationStatus::UP) {
                    /** @psalm-suppress  UnresolvableInclude */
                    require_once $migration->fileName;

                    if (!is_a($migration->className, Migration::class, true)) {
                        throw new LogicException("Class '$migration->className' is invalid or does not exist");
                    }

                    ($migration->className)::down($pdo);

                    $migrationStatusNew = MigrationStatus::DOWN;

                    $timeStart = microtime(true);
                    MigrationLogs::down(
                        $config,
                        $migration->serial,
                    );
                    $runTime = microtime(true) - $timeStart;
                } else {
                    $migrationStatusNew = $migration->status;
                    $runTime = .0;
                }

                $postMigrationCallback && $postMigrationCallback(
                    $migration,
                    $migration->status,
                    $migrationStatusNew,
                    $runTime,
                );

                if ($migration->serial === $targetSerial) {
                    break;
                }
            }
        } catch (Throwable $throwable) {
            $pdo->rollBack();
            throw $throwable;
        }

        $dryRun
            ? $pdo->rollBack()
            : $pdo->commit();
    }

    /**
     * @param Config $config
     * @return array<positive-int, MigrationData>
     */
    public static function getStatus(Config $config): array
    {
        $result = [];

        $migrationFiles = MigrationStorage::list($config);
        $migrationLogs = MigrationLogs::getAll($config);

        $serials = array_merge(
            array_keys($migrationLogs),
            array_keys($migrationFiles),
        );

        foreach ($serials as $serial) {
            $file = $migrationFiles[$serial] ?? null;
            $log = $migrationLogs[$serial] ?? null;

            $status = $file
                ? MigrationLogs::getStatus($config, $serial)
                : MigrationStatus::MISSING;

            $result[$serial] = new MigrationData(
                $serial,
                $file?->name ?? $log?->name ?? 'Unknown',
                $status,
                $log?->timestamp,
                $log?->savepoint,
                $file?->fileName,
                $file?->className,
            );
        }

        ksort($result);

        return $result;
    }
}
