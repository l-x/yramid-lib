<?php

declare(strict_types=1);

namespace Yramid;

use PDO;
use Yramid\Exception\InvalidConfigValue;

/**
 * @internal
 */
final class ConfigAccessor
{
    private const NAMESPACE = '{^\w+(?:\\\\\w+)*$}';

    public static function getPdo(Config $config): PDO
    {
        $pdo = $config->getPdo();

        if ($pdo !== $config->getPdo()) {
            InvalidConfigValue::raise('Config returns different PDO instances');
        }

        return $pdo;
    }

    public static function getMigrationPath(Config $config): string
    {
        return $config->getMigrationPath();
    }

    public static function getMigrationNamespace(Config $config): string
    {
        return self::validNamespace($config->getMigrationNamespace());
    }

    public static function getMigrationTemplate(Config $config): string
    {
        return $config->getMigrationTemplate() ?? __DIR__ . '/Migration/migration.tpl';
    }

    public static function getSeedPath(Config $config): string
    {
        return $config->getSeedPath();
    }

    public static function getSeedNamespace(Config $config): string
    {
        return self::validNamespace($config->getSeedNamespace());
    }

    public static function getSeedTemplate(Config $config): string
    {
        return $config->getSeedTemplate() ?? __DIR__ . '/Seed/seed.tpl';
    }

    private static function validNamespace(string $namespace): string
    {
        if (!preg_match(self::NAMESPACE, $namespace)) {
            InvalidConfigValue::raise('Invalid namespace: ' . $namespace);
        }

        return $namespace;
    }
}
