<?php

declare(strict_types=1);

namespace Yramid\Exception;

class InvalidName extends RuntimeException
{
    public function __construct(string $name)
    {
        parent::__construct(
            "Invalid name for seed or migration: $name",
        );
    }

    public static function raise(string $name): never
    {
        throw new self($name);
    }
}
