<?php

declare(strict_types=1);

namespace Yramid\Exception;

class InvalidConfigValue extends LogicException
{
    public function __construct(string $message)
    {
        parent::__construct(
            "Invalid configuration value: $message",
        );
    }

    public static function raise(string $message): never
    {
        throw new self($message);
    }
}
