<?php

declare(strict_types=1);

namespace Yramid;

use Closure;
use Yramid\Migration\MigrationData;
use Yramid\Seed\SeedData;

/**
 * @api
 */
interface YramidInterface
{
    public function setUp(Config $config): void;

    /**
     * @param Config $config
     * @param non-empty-string $name
     *
     * @return SeedData
     */
    public function createSeed(Config $config, string $name): SeedData;

    /**
     * @param Config $config
     * @param non-empty-string $name
     * @param bool $dryRun
     */
    public function runSeed(Config $config, string $name, bool $dryRun): void;

    /**
     * @param Config $config
     * @param positive-int $serial
     * @param non-empty-string $name
     *
     * @return MigrationData
     */
    public function createMigration(Config $config, int $serial, string $name): MigrationData;

    /**
     * @param Config $config
     * @param positive-int|non-empty-string $target
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     */
    public function migrateUp(
        Config $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void;

    /**
     * @param Config $config
     * @param positive-int|non-empty-string $target
     * @param bool $dryRun
     * @param Closure|null $postMigrationCallback
     */
    public function migrateDown(
        Config $config,
        int|string $target,
        bool $dryRun,
        ?Closure $postMigrationCallback = null,
    ): void;

    /**
     * @param Config $config
     * @param positive-int|non-empty-string $target
     * @param non-empty-string|null $savepoint
     */
    public function setMigrationSavepoint(
        Config $config,
        int|string $target,
        ?string $savepoint,
    ): void;

    /**
     * @param Config $config
     * @return array<positive-int, MigrationData>
     */
    public function getMigrationStatus(Config $config): array;
}
